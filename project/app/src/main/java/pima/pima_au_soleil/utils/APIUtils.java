package pima.pima_au_soleil.utils;

import android.content.Context;
import android.location.Location;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import pima.pima_au_soleil.cafe.AdressController;
import pima.pima_au_soleil.cafe.Cafe;
import pima.pima_au_soleil.cafe.CafeController;
import pima.pima_au_soleil.cafe.CafeSelfController;
import pima.pima_au_soleil.cafe.meteo.OpenWeatherAPIController;
import pima.pima_au_soleil.cafe.terasses.OpenDataAPIController;
import pima.pima_au_soleil.map.MainMapActivity;

/**
 * Classe pour mettre toutes les méthodes utiles qui n'ont pas leur place autre part.
 * Toutes les méthodes ici doivent être statiques.
 */

public class APIUtils {
    /**
     * Le rayon autours duquel on veut la liste des cafés.
     * Il faut pouvoir changer cette valeur plus tard.
     */
    public static final int radius = 800;

    /**
     * Méthode pour faire une requète à l'API.
     * Cette méthode est à privilégier pour faire un appel à l'API.
     * @param context le context de l'activité dans lequel on fait appel à l'API.
     * @param latitude la latitude autour de laquelle faire l'appel.
     * @param longitude la longitude autour de laquelle faire l'appel.
     */
    public static void sendAPIRequest(Context context, double latitude, double longitude) {
        //On crée un nouveau controller et on l'éxécute.
        CafeController controller = new CafeController(context, latitude, longitude, radius);
        controller.start();
    }

    public static void sendGoogleAPIRequest(MainMapActivity mapActivity, Context context, Location loc) {
        AdressController controller = new AdressController(mapActivity, context, loc);
        controller.distanceInfo();

    }

    public static void sendOpenDataAPIRequest(MainMapActivity mapActivity, double latitude, double longitude) {
        OpenDataAPIController controller = new OpenDataAPIController(latitude, longitude, radius, mapActivity);
        controller.start();
    }

    public static void sendOpenWeatherAPIRequest(MainMapActivity mapActivity) {
        OpenWeatherAPIController controller = new OpenWeatherAPIController(mapActivity);
        controller.start();
    }

    public static void sendFindSelfAPIRequest(String link, Context context) {
        CafeSelfController controller = new CafeSelfController(link, context);
        controller.start();
    }

    public static void setDistanceCafes(List<Cafe> cafes, Location location) {
        if (location == null) {
            return;
        } else if (cafes == null) {
            return;
        }
        for (Cafe cafe : cafes) {
            //On calcule la distance entre la position et le cafe
            cafe.setDistance(Double.valueOf(location.getLatitude()), Double.valueOf(location.getLatitude()));
        }
        //On trie les cafés par ordre croissant de distance
        Collections.sort(cafes, new Comparator<Cafe>() {
            @Override
            public int compare(Cafe cafe, Cafe t1) {
                return (int)(cafe.getDistance()*1000 - t1.getDistance()*1000);
            }
        });
    }
}
