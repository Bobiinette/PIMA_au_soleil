package pima.pima_au_soleil.map;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pima.pima_au_soleil.MainApp;
import pima.pima_au_soleil.R;
import pima.pima_au_soleil.cafe.Cafe;
import pima.pima_au_soleil.cafe.CafeController;
import pima.pima_au_soleil.cafe.FindAPI;
import pima.pima_au_soleil.utils.APIUtils;

public class MainMapActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener, LocationListener {
    private static final String TAG = MainMapActivity.class.getSimpleName();
    //Pour la position
    private final int TIME_BETWEEN_UPDATES = 5000; //en millisecondes
    private final int MIN_DISTANCE_BETWEEN_UPDATES = 10; // en secondes
    private static final int NOMBRE_CAFES_AFFICHES = 65;
    private final static String[] LOCALISATION_PERMISSIONS = {
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private final static int LOCATION_REQUEST = 1002;
    private final static int MAP_LOCATION_REQUEST = 1003;
    private LocationManager locationManager;
    private Location location;

    private GoogleMap mMap;
    private Context context;

    private APIBroadcastReceiver receiver;
    private IntentFilter filter;
    private LatLng lastPosition;
    private MainMapActivity thisMap;

    private Map<Cafe, Marker> markers;

    private static boolean isSunny = false;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_map);
        context = this;
        thisMap = this;
        //Pour demander l'autorisation à l'accès de la position
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (Build.VERSION.SDK_INT >= 23) {
            try {
                //On regarde si on a le droit d'accéder à la position
                if (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    requestLocation();
                } else {
                    requestPermissions(LOCALISATION_PERMISSIONS, LOCATION_REQUEST);
                }
            } catch (SecurityException e) {
                Log.d(TAG, "GPS non activé", e);
            }
        } else  {
            requestLocation();
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        filter = new IntentFilter(CafeController.INTENT_BROADCAST_SIGNAL);
        receiver = new APIBroadcastReceiver();

        registerReceiver(receiver,filter);
        APIUtils.sendOpenWeatherAPIRequest(this);
        markers = new HashMap<>();
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean permission = true;
        switch (requestCode) {
            case LOCATION_REQUEST:
                permission = true;
                for (String i : permissions) {
                    permission = permission && hasPermission(i);
                }
                if (permission) {
                    requestLocation();
                } else {
                    Log.w(TAG, "Pas d'accès à " + permissions);
                }
                break;
            case MAP_LOCATION_REQUEST:
                permission = true;
                for (String i : permissions) {
                    permission = permission && hasPermission(i);
                }
                if (permission) {
                    mMap.setMyLocationEnabled(true);
                } else {
                    Log.w(TAG, "Pas d'accès à " + permissions);
                }
                break;
        }
    }

    //On pourra bouger ça un jour dans une classe "utils"
    @TargetApi(23)
    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
    }

    @SuppressLint("MissingPermission")
    private void requestLocation() {
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, TIME_BETWEEN_UPDATES, MIN_DISTANCE_BETWEEN_UPDATES, this);
        if (location == null) {
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }
    }

    private boolean checkGPSEnable() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Setting a custom info window adapter for the google map
        mMap.setInfoWindowAdapter(new InfoWindowAdapter() {

            // Use default InfoWindow frame
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            // Defines the contents of the InfoWindow
            @Override
            public View getInfoContents(Marker marker) {
                // Getting view from the layout file info_window_layout
                View v = getLayoutInflater().inflate(R.layout.info_window_layout, null);

                // Getting the position from the marker
                String snippet = marker.getSnippet();
                final String[] infos = snippet.split(";");
                String title = marker.getTitle();


                // Getting reference to the TextView to set address
                TextView address = (TextView) v.findViewById(R.id.address_cafes);

                // Getting reference to the TextView to set distance
                TextView distance = (TextView) v.findViewById(R.id.distance);

                // Getting reference to the TextView to set distance
                TextView name = (TextView) v.findViewById(R.id.title);

                // Getting reference to the TextView to set information
                TextView lien = (TextView) v.findViewById(R.id.lien);

                // Setting the name
                name.setText(title);

                // Setting the address
                String string = "Adresse:" + infos[1];
                address.setText(string);

                // Setting the distance
                if (infos[0] != null && !infos[0].contains("null")) {
                    string = infos[0];
                    distance.setText(string);
                } else {
                    //Si pas de retour de l'API, pas d'affichage
                    distance.setVisibility(View.GONE);
                }



                String htmlString="<u>voir plus d'informations</u>";
                lien.setText(Html.fromHtml(htmlString));

                // Returning the view containing InfoWindow contents
                return v;

            }
        });

        if (Build.VERSION.SDK_INT >= 23) {
            try {
                //On regarde si on a le droit d'accéder à la position
                if (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    mMap.setMyLocationEnabled(true);
                } else {
                    requestPermissions(LOCALISATION_PERMISSIONS, MAP_LOCATION_REQUEST);
                }
            } catch (SecurityException e) {
                Log.d(TAG, "GPS non activé", e);
            }
        } else {
            mMap.setMyLocationEnabled(true);
        }
        Log.e(TAG, "location " + mMap.isMyLocationEnabled());

        //On définie le boutton pour revenir sur sa position
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                LatLng positionActuelle = new LatLng(location.getLatitude(), location.getLongitude());
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(positionActuelle, 15.0f));
                return false;
            }
        });

        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                if  (lastPosition == null) {
                    lastPosition = mMap.getCameraPosition().target;
                }
                LatLng posCamera = mMap.getCameraPosition().target;
                if (doesSendAPIRequest(lastPosition, posCamera)) {
                    APIUtils.sendAPIRequest(context, posCamera.latitude, posCamera.longitude);
                    lastPosition = posCamera;
                }
            }

            private boolean doesSendAPIRequest(LatLng lastPos, LatLng newPos) {
                Double lat1 = Math.toRadians(lastPos.latitude);
                Double lat2 = Math.toRadians(newPos.latitude);
                Double lon1 = Math.toRadians(lastPos.longitude);
                Double lon2 = Math.toRadians(newPos.longitude);
                return (Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1)) * 6371)*1000 > APIUtils.radius / 3;
            }
        });
        mMap.setOnInfoWindowClickListener(this);
        changeMap();
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        // Getting the informations from the marker
        String snippet = marker.getSnippet();
        final String[] infos = snippet.split(";");

        APIUtils.sendFindSelfAPIRequest(infos[2], this);
    }


    /**
     * Fonction à appeler lors du lancement de l'application.
     * Permet de définir un zoom par défaut.
     */
    private void onFirstTime(LatLng positionActuelle) {
        if (location != null) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(positionActuelle, 15.0f));
            APIUtils.sendAPIRequest(context, location.getLatitude(), location.getLongitude());
        }
    }

    private void changeMap() {
        if (mMap == null) {
            return;
        }

        if (location == null) {
            Toast.makeText(this, "GPS not enable", Toast.LENGTH_SHORT).show();
            return;
        }

        LatLng posActuelle = new LatLng(location.getLatitude(), location.getLongitude());

        Float zoom = mMap.getCameraPosition().zoom;
        Log.i(TAG, "Zoom " + zoom);
        if (zoom < 5) {
            onFirstTime(posActuelle);
        }
    }

    public double dist(LatLng thisPosition, Cafe cafe) {
        Double distance;
        Double lat1 = Math.toRadians(thisPosition.latitude);
        Double lat2 = Math.toRadians(cafe.getLocation().getLat());
        Double lon1 = Math.toRadians(thisPosition.longitude);
        Double lon2 = Math.toRadians(cafe.getLocation().getLon());

        distance = Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1)) * 6371 * 1000;
        return distance;
    }

    public synchronized void updateMarkers() {
        if (mMap == null) return;
        //Log.d(TAG,"------------UPDATE MARKER----------");
        List<Cafe> cafes = MainApp.getCafes();
        synchronized (MainApp.getCafes()) {
            if (lastPosition != null) {
                final LatLng thisPosition = lastPosition;
                Collections.sort(MainApp.getCafes(), new Comparator<Cafe>() {
                    @Override
                    public int compare(Cafe cafe, Cafe t1) {
                        return (int) ((dist(thisPosition, cafe) - dist(thisPosition, t1)));
                    }
                });
                int i = 0;
                while (i < NOMBRE_CAFES_AFFICHES && i < cafes.size()) {
                    Cafe cafe = cafes.get(i);
                    if (dist(thisPosition, cafe) > APIUtils.radius) {
                        Log.d(TAG, "remove some things");
                        if (markers.get(cafe) != null) {
                            markers.get(cafe).setVisible(false);
                            markers.get(cafe).remove();
                        }
                        markers.remove(cafe);
                        cafes.remove(i);
                    } else if (!markers.containsKey(cafe)){
                        LatLng posCafe = new LatLng(cafe.getLocation().getLat(), cafe.getLocation().getLon());
                        String infos = cafe.getDuration() + ";" + cafe.getAddress() + ";" + cafe.getInfosLink();
                        Marker marker;
                        if (cafe.getTerrasse()) {
                            if (cafe.getSoleil()) {
                                marker = mMap.addMarker(new MarkerOptions().position(posCafe).title(cafe.getBusiness_name()).snippet(infos).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
                            } else {
                                marker = mMap.addMarker(new MarkerOptions().position(posCafe).title(cafe.getBusiness_name()).snippet(infos).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                            }
                        } else {
                            marker = mMap.addMarker(new MarkerOptions().position(posCafe).title(cafe.getBusiness_name()).snippet(infos));
                        }
                        markers.put(cafe, marker);
                    }
                    i++;
                }
            }
            //Suppression de la queue de la liste
            if (cafes.size() > NOMBRE_CAFES_AFFICHES) {
                Log.d(TAG, "Suppression de " + (cafes.size() - NOMBRE_CAFES_AFFICHES) + " éléments");
                List<Cafe> toRemove =  cafes.subList(NOMBRE_CAFES_AFFICHES, cafes.size());
                for (Cafe cafe : toRemove) {
                    if (markers.get(cafe) != null) {
                        markers.get(cafe).setVisible(false);
                        markers.get(cafe).remove();
                    }
                    markers.remove(cafe);
                }
                cafes.subList(NOMBRE_CAFES_AFFICHES, cafes.size()).clear();
            }
        }
        Log.d(TAG, "markers lenght " + markers.size());
    }

    public void setSunny(boolean sunny) {
        isSunny = sunny;
        synchronized (MainApp.getCafes()) {
            for (Cafe cafe : MainApp.getCafes()) {
                cafe.setSoleil(sunny);
            }
        }
        updateMarkers();
    }

    public static boolean isSunny() {
        return isSunny;
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i(TAG, "Lat " + location.getLatitude() + " Lon " + location.getLongitude());
        if (!checkGPSEnable()) {
            this.location = null;
            return;
        }
        this.location = location;
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    public class APIBroadcastReceiver extends BroadcastReceiver {
        private static final String TAG = "APIBroadcastReceiver";
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "APIBroadcastReceiver");

            //Pour la phase de test, mettre ça en commentaire
            APIUtils.sendGoogleAPIRequest(thisMap, context,location);
            //Mettre ça en commentaire pour le final
            //updateMarkers();

            APIUtils.sendOpenDataAPIRequest(thisMap, location.getLatitude(), location.getLongitude());
        }

    }

}
