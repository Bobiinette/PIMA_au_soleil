package pima.pima_au_soleil.cafe.meteo;

import pima.pima_au_soleil.cafe.terasses.OpenDataAPIResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by nicolas on 17/12/17.
 */

public interface OpenWeatherAPI {
    @Headers({"Accept: *"})
    @GET("weather")
    Call<OpenWeatherAPIResponse> loadWeather(@Query("id") String id,
                                           @Query("units") String units,
                                           @Query("APPID") String appid,
                                           @Query("lang") String lang);
}
