package pima.pima_au_soleil.cafe;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by puddu on 26/11/17.
 */

public interface GoogleAPI {
    //@Headers({"Accept: *", "Authorization: FIND-TOKEN key=\"AIzaSyBUzKK9B12x1EDfIztD2l0T0hXC7WQutec\""})

    @GET("json")
     Call<AdressAPIResponse> loadadressChanges(@Query("origins") String origins,
                                                     @Query("destinations") String destinations);
}