package pima.pima_au_soleil.cafe;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import pima.pima_au_soleil.MainApp;

/**
 * Classe pour extraires les données obtenues avec le JSON de réponse de l'API Find.
 * On transforme le JSON en variables.
 */

public class CafeAPIResponse {
    private static final String TAG = CafeAPIResponse.class.getSimpleName();
    /**
     * Les différents liens contenus dans le JSON pour avoir la page actuelle, et les pages suivantes et précédentes
     */
    private Links _links;
    /**
     * Les données du JSON. C'est ici que l'on aura les données des cafés.
     */
    private Embedded _embedded;
    /**
     * Le nombre de pages.
     */
    private int page_count;
    /**
     * Le nombre d'éléments (de cafés) sur la page.
     */
    private int page_size;
    /**
     * Le nombre total de cafés sur toutes les pages.
     */
    private int total_items;
    /**
     * La page actuelle.
     */
    private int page;

    /**
     * Getter de {@link #_links}
     * @return {@link #_links}
     */
    public Links get_links() {
        return _links;
    }

    /**
     * Getter de {@link #_embedded}
     * @return {@link #_embedded}
     */
    public Embedded get_embedded() {
        return _embedded;
    }

    /**
     * Getter de {@link #page_count}
     * @return {@link #page_count}
     */
    public int getPage_count() {
        return page_count;
    }

    /**
     * Getter de {@link #page_size}
     * @return {@link #page_size}
     */
    public int getPage_size() {
        return page_size;
    }

    /**
     * Getter de {@link #total_items}
     * @return {@link #total_items}
     */
    public int getTotal_items() {
        return total_items;
    }

    /**
     * Getter de {@link #page}
     * @return {@link #page}
     */
    public int getPage() {
        return page;
    }

    /**
     * Classe pour récupérer les données du JSON.
     * La classe reprend la structure du JSON.
     */
    public class Embedded {
        /**
         * La liste des données qui correspond à la liste des cafés contenue dans le JSON.
         */
        private List<Operator> operator;

        /**
         * Ajoute à une liste de cafées la liste des cafés contenue dans {@link #operator}.
         * Si le paramètre vaut null, on crée une nouvelle liste.
         */
        public void miseAJourCafes(){
            synchronized (MainApp.getCafes()) {
                List<Cafe> cafes = MainApp.getCafes();
                for (Operator opp : operator) {
                    //On parcours les cafés et on crée les nouveaux cafés que l'on ajoute à la liste.
                    if (!estPresent(cafes, opp.location)) {
                        cafes.add(new Cafe(opp.business_name, opp.address, opp.location, opp._links.self.getHref()));
                    }
                }
            }
        }

        private boolean estPresent(List<Cafe> cafes, Cafe.Location locCafe) {
            for (Cafe cc : cafes) {
                if (cc.getLocation().egal(locCafe)) {
                    return true;
                }
            }
            return false;
        }

        private class Operator {
            /**
             * L'adresse du café.
             */
            private String address;
            /**
             * Le nom du café.
             */
            private String business_name;
            /**
             * Le nom de la chaine du café (si le café fait partie d'une chaine).
             */
            private String chain_name;
            /**
             * Le nom de la ville dans laquelle se trouve le café.
             */
            private String city;
            /**
             * Le code du pays dans lequel se trouve le café (ex : "fr").
             */
            private String country;
            /**
             * DErnière mise à jour des informations.
             */
            private String last_update;
            /**
             * Le nom légal.
             */
            private String legal_name;
            /**
             * Sa position avec sa latitude et longitude.
             */
            private Cafe.Location location;
            /**
             * Son code postal.
             */
            private String postal_code;
            /**
             * La région dans laquelle il se trouve.
             */
            private String region1;
            /**
             * Un lien correspondant au café.
             */
            private Links _links;

            /**
             * Méthode toString pour les logs.
             * @return
             */
            @Override
            public String toString() {
                return "Operator{" +
                        "address='" + address + '\'' +
                        ", business_name='" + business_name + '\'' +
                        ", chain_name='" + chain_name + '\'' +
                        ", city='" + city + '\'' +
                        ", country='" + country + '\'' +
                        ", last_update='" + last_update + '\'' +
                        ", legal_name='" + legal_name + '\'' +
                        ", location=" + location +
                        ", postal_code='" + postal_code + '\'' +
                        ", region1='" + region1 + '\'' +
                        ", self=" + _links +
                        '}';
            }
        }

        /**
         * Méthode toString pour les logs.
         * @return
         */
        @Override
        public String toString() {
            return "Embedded{" +
                    "operator=" + operator +
                    '}';
        }
    }

    /**
     * La classe pour gérer les liens contenus dans le JSON.
     */
    public class Links {
        /**
         * Lien vers la page actuelle.
         */
        private LinkAttribute self;
        /**
         * Lien vers la première page.
         */
        private LinkAttribute first;
        /**
         * Lien vers la dernière page.
         */
        private LinkAttribute last;
        /**
         * Lien vers a page suivante.
         */
        private LinkAttribute next;

        public LinkAttribute getNext() {
            return next;
        }

        /**
         * Classe pour gérer un lien (un lien dans le JSON est un objet avec une chaine de caractères dedans).
         */
        public class LinkAttribute {
            /**
             * La chaine de caractères contenue dans le lien.
             */
            private String href;

            /**
             * Getter de {@link #href}
             * @return {@link #href}
             */
            public String getHref() {
                return href;
            }

            /**
             * Setter de {@link #href}
             * @param href la nouvelle valeur de {@link #href}
             */
            public void setHref(String href) {
                this.href = href;
            }

            /**
             * Méthode toString pour les logs.
             * @return
             */
            @Override
            public String toString() {
                return "LinkAttribute{" +
                        "href='" + href + '\'' +
                        '}';
            }
        }

        /**
         * Méthode toString pour les logs.
         * @return
         */
        @Override
        public String toString() {
            return "Links{" +
                    "self=" + self +
                    ", first=" + first +
                    ", last=" + last +
                    ", next=" + next +
                    '}';
        }
    }

    /**
     * Méthode toString pour les logs.
     * @return
     */
    @Override
    public String toString() {
        return "CafeAPIResponse{" +
                "_links=" + _links +
                ", _embedded=" + _embedded +
                ", page_count=" + page_count +
                ", page_size=" + page_size +
                ", total_items=" + total_items +
                ", page=" + page +
                '}';
    }
}
