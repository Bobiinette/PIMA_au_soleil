package pima.pima_au_soleil.cafe;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Interface pour faire les appels A l'API.
 */

public interface FindAPI {

    /**
     * On met en place les Headers de la requète avec le tag @Headers.
     * On fait un Get avec le tag @Get en précisant que l'on spécifie les "operator".
     * On précise le call en lui disant le type de retour, et les éléments à spécifier.
     * @param country le pays dans lequel on fait la requète.
     * @param city la ville dans laquelle on veut les cafés.
     * @param business_name le type de cafés que l'on veut (ex : "bar", "coffee", "cafe").
     * @param lat la latitude autour de laquelle on veut la liste des cafés.
     * @param lon la longitude autour de laquelle on veut la liste des cafés.
     * @param radius le rayon maximal entre notre position et les cafés.
     * @return La réponse de l'API sous forme d'une {@link CafeAPIResponse}
     */
    @Headers({"Accept: *", "Authorization: FIND-TOKEN key=\"$2y$10$cfoox3SsUZLMMS6OrYdO9.fcJKQ9ofkZLZFyIfxcOQ8OCC6Km0tUy\""})
    @GET("operator")
    Call<CafeAPIResponse> loadChanges(@Query("country") String country,
                                      @Query("city") String city,
                                      @Query("business_name") String business_name,
                                      @Query("lat") String lat,
                                      @Query("lon") String lon,
                                      @Query("radius") String radius,
                                      @Query("page") String page);

    @Headers({"Accept: *", "Authorization: FIND-TOKEN key=\"$2y$10$cfoox3SsUZLMMS6OrYdO9.fcJKQ9ofkZLZFyIfxcOQ8OCC6Km0tUy\""})
    @GET("operator/{id}")
    Call<CafeSelfResponse> loadSelf(@Path("id") String id);
}
