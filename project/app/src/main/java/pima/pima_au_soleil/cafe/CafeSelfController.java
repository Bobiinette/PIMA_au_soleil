package pima.pima_au_soleil.cafe;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by nicolas on 18/12/17.
 */

public class CafeSelfController implements Callback<CafeSelfResponse> {
    private static final String TAG = CafeSelfController.class.getSimpleName();
    private String selfLink;
    private String id;
    private Context context;

    public CafeSelfController(String selfLink, Context context) {
        this.selfLink = selfLink.substring(0, selfLink.lastIndexOf('/'));
        this.selfLink = this.selfLink.substring(0, this.selfLink.lastIndexOf('/') + 1);
        id = selfLink.substring(selfLink.lastIndexOf('/') + 1, selfLink.length());
        this.context = context;
    }

    public void start() {
        Gson gson = new GsonBuilder().setLenient().create();
        //On crée l'objet Retrofit pour faire l'appel à l'API.
        //On définie ici les attributs de l'objet.
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(selfLink)
                .build();
        //On crée un objet FindAPI pour faire l'appel à la requète à partir de l'ojet retrofit.
        FindAPI findAPI = retrofit.create(FindAPI.class);
        Call<CafeSelfResponse> call = findAPI.loadSelf(id);
        //On enqueue les requètes, le programme va faire les requètes
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<CafeSelfResponse> call, Response<CafeSelfResponse> response) {
        if (response.isSuccessful()) {
            String tripadvisor = response.body().getSocialTripAdvisor();
            if (tripadvisor != null) {
                Uri webpage = Uri.parse(tripadvisor);
                Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                if (intent.resolveActivity(context.getPackageManager()) != null) {
                    context.startActivity(intent);
                }

            }

        }
    }

    @Override
    public void onFailure(Call<CafeSelfResponse> call, Throwable t) {

    }
}
