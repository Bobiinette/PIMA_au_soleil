package pima.pima_au_soleil.cafe.meteo;

import android.util.Log;

import java.util.List;

/**
 * Created by nicolas on 17/12/17.
 */

public class OpenWeatherAPIResponse {
    private final static String TAG = OpenWeatherAPIResponse.class.getSimpleName();
    private List<Weather> weathers;
    private Sys sys;

    public boolean isSunny() {
        if (weathers == null) {
            return false;
        }
        if (sys == null) {
            return false;
        }
        boolean result = true;
        long time = System.currentTimeMillis();
        if (time < sys.sunrise * 1000 || time > sys.sunset * 1000) {
            return true;
            //result = false;
        }
        for (Weather weather : weathers) {
            result = result && (weather.id.matches("800") || weather.id.matches("801"));
        }
        return result;
    }

    public class Weather {
        private String id;
    }

    public class Sys {
        private long sunrise;
        private long sunset;
    }
}
