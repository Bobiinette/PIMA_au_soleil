package pima.pima_au_soleil.cafe.terasses;

import android.util.Log;

import java.util.Arrays;
import java.util.List;

import pima.pima_au_soleil.MainApp;
import pima.pima_au_soleil.cafe.Cafe;

/**
 * Pour traiter la réponse de l'API Open Data
 */

public class OpenDataAPIResponse {
    private final static String TAG = OpenDataAPIResponse.class.getSimpleName();
    private final static String TERRASSE_OUVERTE = "TERRASSE OUVERTE";
    private final static String TERRASSE_FERMEE = "TERRASSE FERMEE";
    private List<Record> records;
    private int nhits;

    private List<Record> getRecords() {
        return records;
    }

    public void setTerasses() {
        synchronized (MainApp.getCafes()) {
            List<Cafe> cafes = MainApp.getCafes();
            Log.d(TAG, "setTerasse() " + records + " " + nhits);
            for (Record record : records) {
                Log.d(TAG, "libelle_type " + record.libelle_type);
                Log.d(TAG, "[0] [1] : " + record.geo_point_2d.get(0) + " " + record.geo_point_2d.get(1));
                for (Cafe cafe : cafes) {
                    double latRecord = record.getGeo_point_2d().get(1);
                    double lonRecord = record.getGeo_point_2d().get(0);
                    cafe.haveTerrasse(latRecord, lonRecord);
                }
            }
        }
    }

    @Override
    public String toString() {
        return "OpenDataAPIResponse{" +
                "records=" + records +
                '}';
    }

    public class Record {

        private List<Double> geo_point_2d;
        private String libelle_type;

        public List<Double> getGeo_point_2d() {
            return geo_point_2d;
        }

        public String getLibelle_type() {
            return libelle_type;
        }

        @Override
        public String toString() {
            return "Record{" +
                    "geo_point_2d=" + geo_point_2d +
                    ", libelle_type='" + libelle_type + '\'' +
                    '}';
        }
    }
}
