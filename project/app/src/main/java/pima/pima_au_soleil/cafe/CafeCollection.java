package pima.pima_au_soleil.cafe;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nicolas on 20/10/17.
 */

public class CafeCollection {
    private static final String TAG = CafeCollection.class.getSimpleName();

    public static List<Cafe> getCafesFromJson(String path) {
        List<Cafe> cafes = null;
        try {
            JsonParser jsonParser = new JsonParser();
            FileReader reader = new FileReader(path);
            JsonElement jsonElement = jsonParser.parse(reader);
            if (jsonElement.isJsonObject()) {
                JsonObject jsonCafes = jsonElement.getAsJsonObject();
                JsonObject _embedded = jsonCafes.get("_embedded").getAsJsonObject();
                JsonArray operator = _embedded.get("operator").getAsJsonArray();
                cafes = new ArrayList<>();
                for (JsonElement element : operator) {
                    JsonObject object = element.getAsJsonObject();
                    JsonObject location = object.get("location").getAsJsonObject();
                    cafes.add(new Cafe(object.getAsJsonPrimitive("business_name").getAsString(),
                            object.getAsJsonPrimitive("address").getAsString(),
                            location.getAsJsonPrimitive("lon").getAsDouble(),
                            location.getAsJsonPrimitive("lat").getAsDouble()));
                }
            }
        } catch (FileNotFoundException e) {
            Log.e(TAG, "getCafesFromJson/", e);
        }
        return cafes;
    }

    public static List<Cafe> getCafesFromAssets(Context context) {
        List<Cafe> cafes = null;
        try {
            InputStream is = context.getAssets().open("cafes.json");
            java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
            String str = "";
            if (s.hasNext()) {
                str = s.next();
            }
            JsonParser jsonParser = new JsonParser();
            JsonElement jsonElement = jsonParser.parse(str);
            if (jsonElement.isJsonObject()) {
                JsonObject jsonCafes = jsonElement.getAsJsonObject();
                JsonObject _embedded = jsonCafes.get("_embedded").getAsJsonObject();
                JsonArray operator = _embedded.get("operator").getAsJsonArray();
                cafes = new ArrayList<>();
                for (JsonElement element : operator) {
                    JsonObject object = element.getAsJsonObject();
                    JsonObject location = object.get("location").getAsJsonObject();
                    cafes.add(new Cafe(object.getAsJsonPrimitive("business_name").getAsString(),
                            object.getAsJsonPrimitive("address").getAsString(),
                            location.getAsJsonPrimitive("lon").getAsDouble(),
                            location.getAsJsonPrimitive("lat").getAsDouble()));
                }
            }
        } catch (IOException e) {
            Log.e(TAG, "getCafesFromAssets/", e);
        }
        return cafes;
    }
}
