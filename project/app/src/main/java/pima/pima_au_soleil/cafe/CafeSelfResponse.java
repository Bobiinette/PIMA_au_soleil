package pima.pima_au_soleil.cafe;

import android.app.SearchManager;
import android.content.Intent;

/**
 * Created by nicolas on 18/12/17.
 */

public class CafeSelfResponse {
    private Social social;

    public String getSocialTripAdvisor() {
        if (social == null) {
            return null;
        }
        return social.tripadvisor;
    }

    public class Social {
        private String tripadvisor;
    }
}
