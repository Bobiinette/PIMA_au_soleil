package pima.pima_au_soleil.cafe;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import pima.pima_au_soleil.MainApp;
import pima.pima_au_soleil.map.MainMapActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by puddu on 26/11/17.
 */

public class AdressController implements Callback<AdressAPIResponse> {


    private final String TAG = AdressController.class.getSimpleName();
    /**
     * Le nom du signal que l'on envoie à travers l'application pour signaler que l'on a reçu la réponse.
     */
    public static final String INTENT_BROADCAST_SIGNAL = "pima.pima_au_soleil.cafe.AdressController.RESPONSE";
    private final String BASE_URL = "https://maps.googleapis.com/maps/api/distancematrix/";
    //private final String[] businessTypes = {"coffee", "cafe", "bar"};

    private String origins;
    private String destinations;
    private MainMapActivity map;

    /**
     * Le context dans lequel on fait l'appel à la requète.
     */
    private Context context;

    private String loctoString(Cafe.Location loc){
        return Double.toString(loc.getLat())+","+Double.toString(loc.getLon());

    }

    private String loctoString(Location loc){
        return Double.toString(loc.getLatitude())+","+Double.toString(loc.getLongitude());

    }

    public AdressController (MainMapActivity map, Context context,Location orig) {
        this(context,orig);
        this.map = map;
    }


    public AdressController(Context context,Location orig) {
        map = null;
        origins=loctoString(orig);
        destinations="";
        for(Cafe cafe:MainApp.getCafes()){
            destinations=destinations+loctoString(cafe.getLocation())+"|";
        }

        this.context = context;


    }

    /**
     * Méthode à laquelle on fait appel pour faire la requète à l'API Find.
     */
    public void distanceInfo() {
        // http://maps.googleapis.com/maps/api/distancematrix/json?destinations=Atlanta,GA|New+York,NY&origins=Orlando,FL&units=imperial
        Gson gson = new GsonBuilder().setLenient().create();
        //On crée l'objet Retrofit pour faire l'appel à l'API.
        //On définie ici les attributs de l'objet.

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(BASE_URL)
                .build();
        GoogleAPI googleAPI = retrofit.create(GoogleAPI.class);
        Call <AdressAPIResponse> call= googleAPI.loadadressChanges(origins,destinations);
        Log.d(TAG, call.request().headers() + "\n" + call.request().toString());
        call.enqueue(this);
    }

        @Override
    public void onResponse(Call<AdressAPIResponse> call, Response<AdressAPIResponse> response) {
            Log.d(TAG, "reponse " + response.raw());
            //Si on a une bonne réponse de l'API
            if (response.isSuccessful()) {
                //On récupère les informations dans le corps de la réponse
                AdressAPIResponse cafeinfo = response.body();
               /* if (cafeinfo != null) {
                    //On rallonge la liste des cafés
                    cafes = cafeinfo.get_embedded().getCafes(cafes);
                }*/
                Log.i(TAG, "response is successfull " + cafeinfo);
                //On met à jour la liste des cafés dans le MainApp
                synchronized (MainApp.getCafes()) {
                    if (cafeinfo != null) {
                        List<AdressAPIResponse.Rows> row = cafeinfo.getRows();
                        if (row.isEmpty()) {
                            map.updateMarkers();
                            Log.e(TAG, "row.isEmpty()");
                            return;
                        }
                        row.get(0).setDistAndTimes(MainApp.getCafes());

                    }
                }
                if (map != null) {
                    Log.d(TAG,"------preupdate--------");
                    map.updateMarkers();
                } else {
                    Log.e(TAG, "map null");
                }
            } else {
                Log.w(TAG, "reponse failed " + response.raw());
                map.updateMarkers();
            }
    }

    @Override
    public void onFailure(Call<AdressAPIResponse> call, Throwable t) {
        t.printStackTrace();

    }
}
