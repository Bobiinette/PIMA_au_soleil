package pima.pima_au_soleil.cafe.meteo;

import android.location.Location;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import pima.pima_au_soleil.MainApp;
import pima.pima_au_soleil.cafe.Cafe;
import pima.pima_au_soleil.cafe.terasses.OpenDataAPIResponse;
import pima.pima_au_soleil.map.MainMapActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by nicolas on 17/12/17.
 */

public class OpenWeatherAPIController implements Callback<OpenWeatherAPIResponse> {
    private final static String TAG = OpenWeatherAPIController.class.getSimpleName();
    private final String BASE_URL = "http://api.openweathermap.org/data/2.5/";
    private final String id = "6455259";
    private final String units = "metric";
    private final String APPID = "a749c0cce51670edc5c16520977b9cb5";
    private final String lang = "fr";
    private MainMapActivity mapActivity;

    public OpenWeatherAPIController(MainMapActivity mapActivity) {
        this.mapActivity = mapActivity;
    }

    public void start() {
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(BASE_URL)
                .build();
        OpenWeatherAPI openDataAPI = retrofit.create(OpenWeatherAPI.class);
        Call<OpenWeatherAPIResponse> call = openDataAPI.loadWeather(id,
                units,
                APPID,
                lang);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<OpenWeatherAPIResponse> call, Response<OpenWeatherAPIResponse> response) {
        Log.d(TAG, "reponse " + response.raw());
        if(response.isSuccessful()) {
            OpenWeatherAPIResponse dataAPIResponse = response.body();
            if(dataAPIResponse != null) {
                mapActivity.setSunny(dataAPIResponse.isSunny());
            }
        } else {
            Log.e(TAG, "reponse failed : " + response.errorBody());
        }
    }

    @Override
    public void onFailure(Call<OpenWeatherAPIResponse> call, Throwable t) {

    }
}
