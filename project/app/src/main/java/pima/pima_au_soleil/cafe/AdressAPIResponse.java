package pima.pima_au_soleil.cafe;

/**
 * Created by puddu on 27/11/17.
 */

import android.nfc.Tag;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import java.util.ArrayList;
import java.util.List;

import pima.pima_au_soleil.MainApp;
import pima.pima_au_soleil.map.MainMapActivity;

/**
 * Classe pour extraires les données obtenues avec le JSON de réponse de l'API Find.
 * On transforme le JSON en variables.
 */

public class AdressAPIResponse {


    /**
     * Les différents liens contenus dans le JSON pour avoir la page actuelle, et les pages suivantes et précédentes
     */


    private List<Rows> rows;



    public List<Rows> getRows() {
        return rows;
    }

    /**
     * Classe pour récupérer les données du JSON.
     * La classe reprend la structure du JSON.
     */
    public class Rows {
        /**
         * La liste des données qui correspond à la liste des cafés contenue dans le JSON.
         */
        private List<Elements> elements;


        public void setDistAndTimes(List<Cafe> listeARemplir){
            synchronized (MainApp.getCafes()) {
                Double[] tab = {0.0, 0.0};
                int i = 0;
                Elements buf;
                for (Cafe cafe : listeARemplir) {
                    //On parcours les cafés et on crée les nouveaux cafés que l'on ajoute à la liste.
                    buf = elements.get(i);
                    //cafe.setDistance2(buf.getDistance2().getText());
                    cafe.setDuration(buf.getDuration().getText());
                    i++;
                    if (i >= elements.size()) {
                        break;
                    }
                }
            }
        }

        public class Elements {


            /**
             * L'adresse du café.
             */
            private Distance distance;
            /**
             * Le nom du café.
             */
            private Duration duration;

            private String status;

            public Distance getDistance() {
                return distance;
            }

            public Duration getDuration() {
                return duration;
            }

            @Override
            public String toString() {
                return "Elements{" +
                        "distance=" + distance +
                        ", duration=" + duration +
                        ", status='" + status + '\'' +
                        '}';
            }

            public class Distance{


                private String text;
                private String value;

                public String getText() {
                    return text;
                }

                @Override
                public String toString() {
                    return "Distance{" +
                            "text='" + text + '\'' +
                            ", value='" + value + '\'' +
                            '}';
                }
            }

            public class Duration{
                private String text;

                public String getText() {
                    return text;
                }

                private String value;

                @Override
                public String toString() {
                    return "Duration{" +
                            "text='" + text + '\'' +
                            ", value='" + value + '\'' +
                            '}';
                }
            }
        }



    }

}

