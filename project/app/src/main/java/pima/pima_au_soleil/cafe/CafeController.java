package pima.pima_au_soleil.cafe;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import pima.pima_au_soleil.MainApp;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by nicolas on 14/11/17.
 */

public class CafeController implements Callback<CafeAPIResponse> {
    private final String TAG = CafeController.class.getSimpleName();
    /**
     * Le nom du signal que l'on envoie à travers l'application pour signaler que l'on a reçu la réponse.
     */
    public static final String INTENT_BROADCAST_SIGNAL = "pima.pima_au_soleil.cafe.CafeController.RESPONSE";
    private final String BASE_URL = "https://find-api.chd-expert.us/v1/api/";
    private final String[] businessTypes = {"coffee", "cafe", "bar"};
    private String country = "fr";
    private String city = "paris";
    private String lat = "";
    private String lon = "";
    private String radius = "";
    private int numeroPage = 1;
    private String page = "1";
    private int nombreAppelsRestants = 0;
    /**
     * Le context dans lequel on fait l'appel à la requète.
     */
    private Context context;

    /**
     * Le constructeur du controller.
     * @param context le context dans lequel on fait l'appel.
     * @param lat la latitude autour de laquelle faire la requète.
     * @param lon la longitude autour de laquelle faire la requète.
     * @param radius le rayon max que l'on veut entre la position et les cafés.
     */
    public CafeController(Context context, Double lat, Double lon, Integer radius) {
        this.lat = Double.toString(lat);
        this.lon = Double.toString(lon);
        //On transforme le rayon en chaine de caractères.
        this.radius = Integer.toString(radius);
        this.context = context;
    }

    /**
     * Le constructeur du controller.
     * @param context le context dans lequel on fait l'appel.
     * @param lat la latitude autour de laquelle faire la requète.
     * @param lon la longitude autour de laquelle faire la requète.
     * @param radius le rayon max que l'on veut entre la position et les cafés.
     * @param page la page que l'on veut.
     */
    public CafeController(Double lat, Double lon, int radius, int page, Context context) {
        this.lat = Double.toString(lat);
        this.lon = Double.toString(lon);
        this.radius = Integer.toString(radius);
        this.numeroPage = page;
        this.page = Integer.toString(page);
        this.context = context;
    }

    /**
     * Méthode à laquelle on fait appel pour faire la requète à l'API Find.
     */
    public void start() {
        //On crée le gson pour récupérer le JSOn de réponse.
        Gson gson = new GsonBuilder().setLenient().create();
        //On crée l'objet Retrofit pour faire l'appel à l'API.
        //On définie ici les attributs de l'objet.
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(BASE_URL)
                .build();
        //On crée un objet FindAPI pour faire l'appel à la requète à partir de l'ojet retrofit.
        FindAPI findAPI = retrofit.create(FindAPI.class);
        for (String business_name : businessTypes) {
            //O fait les 1 appel pour chaque catégoie de business_name
            Call<CafeAPIResponse> call = findAPI.loadChanges(country,
                    city,
                    business_name,
                    lat,
                    lon,
                    radius,
                    page);
            Log.d(TAG, call.request().headers() + "\n" + call.request().toString());
            //On enqueue les requètes, le programme va faire les requètes
            call.enqueue(this);
            nombreAppelsRestants += 1;
        }
    }

    /**
     * Méthode qui est appelée lorsque l'on reçoit une réponse de l'API.
     * @param call l'appel à l'API.
     * @param response la réponse de l'API.
     */
    @Override
    public void onResponse(Call<CafeAPIResponse> call, Response<CafeAPIResponse> response) {
        Log.d(TAG, "reponse " + response.raw());
        //Si on a une bonne réponse de l'API
        if (response.isSuccessful()) {
            nombreAppelsRestants -= 1;
            //On récupère les informations dans le corps de la réponse
            CafeAPIResponse cafeCollections = response.body();
            if (cafeCollections != null) {
                //On rallonge la liste des cafés
                cafeCollections.get_embedded().miseAJourCafes();
                if (cafeCollections.get_links().getNext() != null) {
                    CafeController nextPage = new CafeController(Double.valueOf(lat), Double.valueOf(lon), Integer.valueOf(radius), numeroPage + 1, context);
                    nextPage.start();
                }
            }
            Log.i(TAG, "response is successfull " + cafeCollections);
            //On envoie un signal pour dire que les cafés ont été changés
            if (nombreAppelsRestants == 0) {
                Intent intent = new Intent();
                intent.setAction(INTENT_BROADCAST_SIGNAL);
                context.sendBroadcast(intent);
            }
        } else {
            if (!call.isExecuted()) {
                nombreAppelsRestants -= 1;
            }
            if (call.isCanceled()) {
                nombreAppelsRestants -= 1;
            }
            Log.w(TAG, "reponse failed " + response.raw() + "\n----------" + call.isExecuted() + "----------");
        }
    }

    @Override
    public void onFailure(Call<CafeAPIResponse> call, Throwable t) {
        t.printStackTrace();
    }
}
