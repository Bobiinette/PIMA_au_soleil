package pima.pima_au_soleil.cafe.terasses;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import pima.pima_au_soleil.cafe.CafeAPIResponse;
import pima.pima_au_soleil.cafe.FindAPI;
import pima.pima_au_soleil.map.MainMapActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by nicolas on 09/12/17.
 */

public class OpenDataAPIController implements Callback<OpenDataAPIResponse> {
    private final static String TAG = OpenDataAPIController.class.getSimpleName();
    private final String BASE_URL = "https://opendata.paris.fr/api/records/1.0/search/";
    private String dataset = "etalages-et-terrasses";
    private String rows = "999";
    private List<String> facet;
    private String geofilter;
    private double latitude;
    private double longitude;
    private int distance;
    private MainMapActivity mapActivity;

    public OpenDataAPIController(double latitude, double longitude, int distance, MainMapActivity mapActivity) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.distance = distance;
        this.mapActivity = mapActivity;
        facet = new ArrayList<>();
        facet.add("libelle_type");
        facet.add("red_profession");
        facet.add("type_lieu1");
        facet.add("type_lieu2");
        facet.add("lateralite");
        facet.add("longueur");
        facet.add("largeurmin");
        facet.add("largeurmax");
        facet.add("date_periode");
        geofilter = Double.toString(latitude) + "," + Double.toString(longitude) + "," + Integer.toString(distance);
    }

    public void start() {
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(BASE_URL)
                .build();
        OpenDataAPI openDataAPI = retrofit.create(OpenDataAPI.class);
        Call<OpenDataAPIResponse> call = openDataAPI.loadTerasses(dataset,
                rows,
                facet,
                geofilter);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<OpenDataAPIResponse> call, Response<OpenDataAPIResponse> response) {
        Log.d(TAG, "reponse " + response.raw());
        if(response.isSuccessful()) {
            OpenDataAPIResponse dataAPIResponse = response.body();
            if(dataAPIResponse != null) {
                dataAPIResponse.setTerasses();
            }
        } else {
            Log.e(TAG, "reponse failed : " + response.errorBody());
        }
    }

    @Override
    public void onFailure(Call<OpenDataAPIResponse> call, Throwable t) {

    }
}
