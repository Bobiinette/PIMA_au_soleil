package pima.pima_au_soleil.cafe;


import android.location.Location;
import android.util.Log;

import pima.pima_au_soleil.map.MainMapActivity;

/**
 * Created by hanna_benj on 16/10/17.
 */

public class Cafe {
    private String business_name;
    private String address;
    private Location location;
    private Double distance;
    private String distance2;
    private String infosLink;


    private String duration;

    private Boolean terrasse = false;
    private Boolean soleil = MainMapActivity.isSunny();

    public Cafe(String business_name, String address, Location location, String infosLink) {
        this(business_name, address, location);
        this.infosLink = infosLink;
    }

    public Cafe(String business_name, String address, Location location) {
        this.business_name = business_name;
        this.address = address;
        this.location = location;
    }

    public Cafe(String business_name, String address, double lon, double lat) {
        this.business_name = business_name;
        this.address = address;
        this.location = new Location(lon, lat);
    }

    public void setTerrasse(Boolean terrasse) {
        this.terrasse = terrasse;
    }

    public Boolean getTerrasse() {
        return terrasse;
    }

    public String getInfosLink() {
        return infosLink;
    }

    public String getDuration() {
        //Log.i("TIME","-------get duration---------");
        return duration;
    }

    public String getDistance2() {
        return distance2;
    }

    public Boolean getSoleil(){
        return soleil;
    }

    public void setSoleil(Boolean soleil) {
        this.soleil = soleil;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double lat, Double lon) {
        Double lat1 = Math.toRadians(location.getLat());
        Double lat2 = Math.toRadians(lat);
        Double lon1 = Math.toRadians(location.getLon());
        Double lon2 = Math.toRadians(lon);

        this.distance = Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1)) * 6371;
    }

    public void setDistance2(String dist){
        distance2=(dist);
    }

    public void setDuration(String dura) {
        //Log.i("CAFE", "------------------SET DURATION " + dura + "------------------");
        duration=dura;
    }

    public String getBusiness_name() {
        return business_name;
    }

    public String getAddress() {
        return address;
    }

    public Location getLocation() {
        return location;
    }

    public void haveTerrasse(double lat_terrasse, double lon_terasse){
        Boolean have_terrasse;
        Double difference;
        Double lat1 = Math.toRadians(location.getLat());
        Double lat2 = Math.toRadians(lat_terrasse);
        Double lon1 = Math.toRadians(location.getLon());
        Double lon2 = Math.toRadians(lon_terasse);

        difference = Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1)) * 6371;
        if (difference < 0.005){
            this.terrasse = true;
        }
    }


    @Override
    public String toString() {
        return "Cafe{" +
                "business_name='" + business_name + '\'' +
                ", address='" + address + '\'' +
                ", location=" + location +
                ", distance=" + distance +
                '}';
    }

    public class Location {
        private double lon;
        private double lat;

        public Location(double lon, double lat) {
            this.lon = lon;
            this.lat = lat;
        }

        public double getLon() {
            return lon;
        }

        public double getLat() {
            return lat;
        }

        public boolean egal(Location loc) {
            return this.lat == loc.lat && this.lon == loc.lon;
        }

        @Override
        public String toString() {
            return "Location{" +
                    "lon=" + lon +
                    ", lat=" + lat +
                    '}';
        }
    }


}
