package pima.pima_au_soleil.cafe.terasses;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Interface pour faire les appels A l'API.
 */

public interface OpenDataAPI {
    @Headers({"Accept: *"})
    @GET(" ")
    Call<OpenDataAPIResponse> loadTerasses(@Query("dataset") String dataset,
                                      @Query("rows") String rows,
                                      @Query("facet") List<String> facet,
                                      @Query("geofilter.distance") String geofilter);
}
