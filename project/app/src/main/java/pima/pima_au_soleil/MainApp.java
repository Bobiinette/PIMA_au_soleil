package pima.pima_au_soleil;

import android.app.Application;
import android.content.Context;
import android.location.Location;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.LogRecord;

import pima.pima_au_soleil.cafe.Cafe;
import pima.pima_au_soleil.cafe.CafeCollection;
import pima.pima_au_soleil.cafe.CafeController;

/**
 * On fait appel à cette classe au lancement de l'application.
 * C'est ici que l'on va avoir pleins de variables statiques et de méthodes dont on fera appel à travers toute l'application
 */

public class MainApp extends Application {
    private static final String TAG = MainApp.class.getSimpleName();
    /**
     * La liste des cafés que l'on exploite à traveurs l'application.
     */
    private final static List<Cafe> cafes = new ArrayList<>();

    @Override
    public void onCreate() {
        super.onCreate();
    }

    /**
     * Getter de {@link #cafes}. Si {@link #cafes} vaut null, on renvoie une nouvelle liste.
     * @return {@link #cafes}.
     */
    public static List<Cafe> getCafes() {
        return cafes;
    }
}
