# Rapport d'itération  
*Ce rapport est à fournir pour chaque équipe au moment du changement d'itération.*

## Composition de l'équipe 
*Remplir ce tableau avec la composition de l'équipe et les rôles.*

|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | *Alexis*                 |
| **Scrum Master**        | *Hanna*                 |

## Bilan de l'itération précédente  
### Évènements 
*Quels sont les évènements qui ont marqué l'itération précédente? Répertoriez ici les évènements qui ont eu un impact sur ce qui était prévu à l'itération précédente.*
> **


### Taux de complétion de l'itération  
*Quel est le nombre d'éléments terminés par rapport au nombre total d'éléments prévu pour l'itération?*
> Répondre ici


### Liste des User Stories terminées
*Quelles sont les User Stories qui ont été validées par le PO à la fin de l'itération ?*

## Rétrospective de l'itération précédente
  
### Bilans des retours et des précédentes actions 
*Quels sont les retours faits par l'équipe pendant la rétrospective? Quelles sont les actions qui ont apporté quelque chose ou non?*
> Répondre ici

### Actions prises pour la prochaine itération
*Quelles sont les actions décidées par l'équipe pour la prochaine itération ?*
Date des stand-up:lundi, mercredi et vendredi
 
### Axes d'améliorations 
*Quels sont les axes d'améliorations pour les personnes qui ont tenu les rôles de PO, SM et Dev sur la précédente itération?*
> Faire plus de schéma pour s'entendre sur l'interface de l'application.

## Prévisions de l'itération suivante  
### Évènements prévus  
*Quels sont les évènements qui vont peut être impacter l'itération? Répertoriez ici les évènements que vous anticipez comme ayant un impact potentiel pour l'itération (absences, changement de cap, -, etc.).*
> 


### Titre des User Stories reportées  
*Lister ici les éléments des itérations précédentes qui ont été reportés à l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*
> Répondre ici

### Titre des nouvelles User Stories  
*Lister ici les nouveaux éléments pour l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*
> ETQ utilisateur, QUAND j’accède à l'application, JE VEUX avoir la liste des cafés les plus proches
ETQ utilisateur, QUAND je vois un café dans la liste, JE VEUX voir à quelle distance/temps de ma position il se trouve
ETQ utilisateur, QUAND je vois un café dans la liste, JE VEUX avoir l'adresse de ce café
ETQ utilisateur, QUAND j’accède à l'application et que je vois la liste des cafés,JE VEUX avoir un bouton pour pour accéder à une map et voir la position des cafés par rapport à ma position
ETQ utilisateur, QUAND je clique sur un café dans la map, JE VEUX qu'un cadre d'information s'ouvre avec dessus le nom du café, l'adresse et la distance par rapport à ma position.

## Confiance 
### Taux de confiance de l'équipe dans l'itération  
*Remplir le tableau sachant que :D est une confiance totale dans le fait de livrer les éléments de l'itération. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 11 	|  *0* 	|  *5* 	    |  *1* 	|  *0* 	|

### Taux de confiance de l'équipe pour la réalisation du projet 
*Remplir le tableau sachant que :D est une confiance totale dans le fait de réaliser le projet. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 11 	|  *0* 	|  *1* 	    |  *5* 	|  *0* 	|

 
 
