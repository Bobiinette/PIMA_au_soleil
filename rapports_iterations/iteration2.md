# Rapport d'itération  
*Ce rapport est à fournir pour chaque équipe au moment du changement d'itération.*

## Composition de l'équipe
*Remplir ce tableau avec la composition de l'équipe et les rôles.*

|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | *Alexis*                 |
| **Scrum Master**        | *Hanna*                 |

## Bilan de l'itération précédente  
### Évènements
*Quels sont les évènements qui ont marqué l'itération précédente? Répertoriez ici les évènements qui ont eu un impact sur ce qui était prévu à l'itération précédente.*
> Une semaine de vacances, pas eu d'avancement, pas fais de stand up.


### Taux de complétion de l'itération  
*Quel est le nombre d'éléments terminés par rapport au nombre total d'éléments prévu pour l'itération?*
> 1/6


### Liste des User Stories terminées
*Quelles sont les User Stories qui ont été validées par le PO à la fin de l'itération ?*
ETQ utilisateur, QUAND je vois un café dans la liste, JE VEUX avoir l'adresse de ce café


## Rétrospective de l'itération précédente

### Bilans des retours et des précédentes actions
*Quels sont les retours faits par l'équipe pendant la rétrospective? Quelles sont les actions qui ont apporté quelque chose ou non?*
> L'équipe apprécis se retrouver pour travailler ensemble.
> Manque de communication fortement souligné. Pour y remédier on a décider de décrire nos actions dans le discord mis en place et aussi d'être plus explicite dans les stand-up.
> Détailler plus précisément les tâches à accomplir et mieux les répartir entre les membres.

### Actions prises pour la prochaine itération
*Quelles sont les actions décidées par l'équipe pour la prochaine itération ?*
Date des stand-up:lundi, jeudi (mais la semaine du 6 il y a forum donc stand up vendredi)
Tous les lundi l'équipe se retrouve pour travailler ensemble
Décrire les actions réaliser dans le discord.

### Axes d'améliorations
*Quels sont les axes d'améliorations pour les personnes qui ont tenu les rôles de PO, SM et Dev sur la précédente itération?*
> La communication comme préciser précédement

## Prévisions de l'itération suivante  
### Évènements prévus  
*Quels sont les évènements qui vont peut être impacter l'itération? Répertoriez ici les évènements que vous anticipez comme ayant un impact potentiel pour l'itération (absences, changement de cap, -, etc.).*
> Forum jeudi 9 donc déplacement du stand up au vendredi 10.


### Titre des User Stories reportées  
*Lister ici les éléments des itérations précédentes qui ont été reportés à l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*
> ETQ utilisateur, QUAND je suis sur la map, JE VEUX voir la position des cafés
ETQ utilisateur, QUAND je suis sur la map, JE VEUX voir ma position
ETQ utilisateur, QUAND j’accède à l'application, JE VEUX avoir la liste des cafés les plus proches
ETQ utilisateur, QUAND je vois un café dans la liste, JE VEUX voir à quelle distance/temps de ma position il se trouve


### Titre des nouvelles User Stories  
*Lister ici les nouveaux éléments pour l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*
>On doit d'abord bien mettre en place les USER STORY précédente.

## Confiance
### Taux de confiance de l'équipe dans l'itération  
*Remplir le tableau sachant que :D est une confiance totale dans le fait de livrer les éléments de l'itération. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 11 	|  *0* 	|  *2* 	    |  *3* 	|  *0* 	|

Un membre absent.

### Taux de confiance de l'équipe pour la réalisation du projet
*Remplir le tableau sachant que :D est une confiance totale dans le fait de réaliser le projet. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 11 	|  *0* 	|  *2* 	    |  *3* 	|  *0* 	|

Un membre absent.

