# Rapport d'itération  
*Ce rapport est à fournir pour chaque équipe au moment du changement d'itération.*

## Composition de l'équipe
*Remplir ce tableau avec la composition de l'équipe et les rôles.*

|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | *Nicolas*                |
| **Scrum Master**        | *Hanna*                  |

## Bilan de l'itération précédente  
### Évènements
*Quels sont les évènements qui ont marqué l'itération précédente? Répertoriez ici les évènements qui ont eu un impact sur ce qui était prévu à l'itération précédente.*
>API Find qui était indisponible pendant environ une semaine et en particulier le lundi où on se retrouve pour travailler


### Taux de complétion de l'itération  
*Quel est le nombre d'éléments terminés par rapport au nombre total d'éléments prévu pour l'itération?*
> 2/6
les 2 autres sont en cours.

### Liste des User Stories terminées
*Quelles sont les User Stories qui ont été validées par le PO à la fin de l'itération ?*
>ETQ utilisateur, QUAND je suis sur la map, JE VEUX voir les cafés autour de la position que je regarde


## Rétrospective de l'itération précédente

### Bilans des retours et des précédentes actions
*Quels sont les retours faits par l'équipe pendant la rétrospective? Quelles sont les actions qui ont apporté quelque chose ou non?*
> Stand up réussi => à continuer
>Bonne répartition des tâches => à continuer
>Travail le dimanche à améliorer

### Actions prises pour la prochaine itération
*Quelles sont les actions décidées par l'équipe pour la prochaine itération ?*
>Se concentrer plus sur l'ensoleillement
>abandonner la liste
>se retrouver samedi 9 Décembre
>mettre à jour le Trello

### Axes d'améliorations
*Quels sont les axes d'améliorations pour les personnes qui ont tenu les rôles de PO, SM et Dev sur la précédente itération?*
> Scrum master bon suivie des stand up
> Tout le monde est venu en Stand up

## Prévisions de l'itération suivante  
### Évènements prévus  
*Quels sont les évènements qui vont peut être impacter l'itération? Répertoriez ici les évènements que vous anticipez comme ayant un impact potentiel pour l'itération (absences, changement de cap, -, etc.).*
>Find si il replante


### Titre des User Stories reportées  
*Lister ici les éléments des itérations précédentes qui ont été reportés à l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*
>ETQ utilisateur, QUAND je vois un café dans la liste, JE VEUX voir à quelle distance/temps de ma position il se trouve
>ETQ utilisateur, QUAND je vois un café map, JE VEUX savoir s'il a une terrasse
>ETQ utilisateur, QUAND je clique sur un café, JE VEUX avoir les bâtiments autour.
>ETQ utilisateur, QUAND je clique sur un café dans la map, JE VEUX qu'un cadre d'information s'ouvre avec dessus le nom du café, l'adresse et le type de cafés.


### Titre des nouvelles User Stories  
*Lister ici les nouveaux éléments pour l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*
>ETQ utilisateur, QUAND je suis sur la map, JE VEUX voir un nombre imité de café: les plus proches de là ou je regarde
>ETQ utilisateur, QUAND je vois regarde les café map, JE VEUX savoir si il y a du soleil


## Confiance
### Taux de confiance de l'équipe dans l'itération  
*Remplir le tableau sachant que :D est une confiance totale dans le fait de livrer les éléments de l'itération. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 11 	|  *0* 	|  *3* 	    |  *3* 	|  *0* 	|


### Taux de confiance de l'équipe pour la réalisation du projet
*Remplir le tableau sachant que :D est une confiance totale dans le fait de réaliser le projet. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 11 	|  *0* 	|  *3* 	    |  *3* 	|  *0* 	|

