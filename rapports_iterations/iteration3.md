# Rapport d'itération  
*Ce rapport est à fournir pour chaque équipe au moment du changement d'itération.*

## Composition de l'équipe
*Remplir ce tableau avec la composition de l'équipe et les rôles.*

|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | *Nicolas*                |
| **Scrum Master**        | *Hanna*                  |

## Bilan de l'itération précédente  
### Évènements
*Quels sont les évènements qui ont marqué l'itération précédente? Répertoriez ici les évènements qui ont eu un impact sur ce qui était prévu à l'itération précédente.*
>Pas fais les stand up
> semaine chargée en controle et rendu de projet dans les autres matières


### Taux de complétion de l'itération  
*Quel est le nombre d'éléments terminés par rapport au nombre total d'éléments prévu pour l'itération?*
> 4/6
les 2 autres sont en cours

### Liste des User Stories terminées
*Quelles sont les User Stories qui ont été validées par le PO à la fin de l'itération ?*
ETQ utilisateur, QUAND je suis sur la map, JE VEUX voir ma position
ETQ utilisateur, QUAND je suis sur la map, JE VEUX voir la position des cafés
ETQ Parisien, afin de me rendre au café, je veux voir où se situe le café sur une carte
ETQ utilisateur, QUAND je vois un café dans la liste, JE VEUX avoir l'adresse de ce café



## Rétrospective de l'itération précédente

### Bilans des retours et des précédentes actions
*Quels sont les retours faits par l'équipe pendant la rétrospective? Quelles sont les actions qui ont apporté quelque chose ou non?*
> L'équipe apprécis se retrouver pour travailler ensemble.
> les stand up ne sont pas encore fait: il manque toujours des membres de l'équipee

### Actions prises pour la prochaine itération
*Quelles sont les actions décidées par l'équipe pour la prochaine itération ?*
Date des stand-up:lundi, jeudi avec rappel oral et sms pour avoir tous les memebres de l'équipe
Tous les lundi l'équipe se retrouve pour travailler ensemble, les lundis consacrés aux rétrospectives et grooming, l'équipe se retrouve le dimanche après midi à la place
Communiquer plus sur le discord, si on a une question la poser dessus, si on a réaliser une tâche l'informé sur le discord.
Asigner les tâches de manière plus précise sur le Trello

### Axes d'améliorations
*Quels sont les axes d'améliorations pour les personnes qui ont tenu les rôles de PO, SM et Dev sur la précédente itération?*
> Faire les rituels : STAND UP!!!!!
> Scrum master rappel plus les stand up

## Prévisions de l'itération suivante  
### Évènements prévus  
*Quels sont les évènements qui vont peut être impacter l'itération? Répertoriez ici les évènements que vous anticipez comme ayant un impact potentiel pour l'itération (absences, changement de cap, -, etc.).*
>


### Titre des User Stories reportées  
*Lister ici les éléments des itérations précédentes qui ont été reportés à l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*
>ETQ utilisateur, QUAND j’accède à l'application, JE VEUX avoir la liste des cafés les plus proches
>ETQ utilisateur, QUAND je vois un café dans la liste, JE VEUX voir à quelle distance/temps de ma position il se trouve


### Titre des nouvelles User Stories  
*Lister ici les nouveaux éléments pour l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*
>ETQ utilisateur, QUAND je suis sur la map, JE VEUX voir les cafés autour de la position que je regarde
>ETQ utilisateur, QUAND je clique sur un café dans la map, JE VEUX qu'un cadre d'information s'ouvre avec dessus le nom du café, l'adresse et le type de cafés.
>ETQ utilisateur, QUAND je vois un café (liste et map), JE VEUX savoir s'il a une terrasse
>ETQ utilisateur, QUAND je clique sur un café, JE VEUX avoir les bâtiments autour.

## Confiance
### Taux de confiance de l'équipe dans l'itération  
*Remplir le tableau sachant que :D est une confiance totale dans le fait de livrer les éléments de l'itération. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 11 	|  *0* 	|  *3* 	    |  *3* 	|  *0* 	|


### Taux de confiance de l'équipe pour la réalisation du projet
*Remplir le tableau sachant que :D est une confiance totale dans le fait de réaliser le projet. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 11 	|  *0* 	|  *1* 	    |  *4* 	|  *1* 	|

