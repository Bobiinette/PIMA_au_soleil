# Rapport d'itération  
*Ce rapport est à fournir pour chaque équipe au moment du changement d'itération.*

## Composition de l'équipe
*Remplir ce tableau avec la composition de l'équipe et les rôles.*

|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | *Nicolas*                |
| **Scrum Master**        | *Hanna*                  |

## Bilan de l'itération précédente  
### Évènements
*Quels sont les évènements qui ont marqué l'itération précédente? Répertoriez ici les évènements qui ont eu un impact sur ce qui était prévu à l'itération précédente.*
> API OpenData indisponible deux jours avant la démo. Les stands up que nous n'avons pas réussi à faire durant ce sprint.


### Taux de complétion de l'itération  
*Quel est le nombre d'éléments terminés par rapport au nombre total d'éléments prévu pour l'itération?*
> 5/6
On a rajouté au cours du sprint trois tickets que nous avons terminés. Un ticket prévu semblait irréalisable au cours du sprint et a été abandonné.

### Liste des User Stories terminées
*Quelles sont les User Stories qui ont été validées par le PO à la fin de l'itération ?*
> ETQ utilisateur, QUAND je clique sur un café dans la map, JE VEUX qu'un cadre d'information s'ouvre avec dessus le nom du café, l'adresse et le type de cafés.
> ETQ utilisateur, QUAND je vois un café sur la map, JE VEUX voir à quelle distance/temps de ma position il se trouve.
> ETQ utilisateur, QUAND je suis sur la map, JE VEUX voir un nombre imité de café: les plus proches de là ou je regarde.
> ETQ utilisateur, QUAND je vois regarde les café map, JE VEUX savoir si il y a du soleil.
> ETQ utilisateur, QUAND je vois un café (liste et map), JE VEUX savoir s'il a une terrasse.
> ETQ utilisateur, QUAND je clique sur un café (liste/map?), JE VEUX avoir des commentaires et recommendations
> ETQ utilisateur, QUAND je clique sur un café (liste/map?), JE VEUX avoir sa fourchette de prix
> ETQ utilisateur, QUAND je clique sur un café (liste/map), JE VEUX avoir les réductions qu'il propose


## Rétrospective de l'itération précédente

### Bilans des retours et des précédentes actions
*Quels sont les retours faits par l'équipe pendant la rétrospective? Quelles sont les actions qui ont apporté quelque chose ou non?*
> Bonne répartition des tâches => à continuer
> Le travail en groupe pendant le week-end a été apprécié par l'équipe => à continuer
> Les stands up n'ont pas été fait
> L'API OpenData ne marchait pas deux jours avant la démo

### Axes d'améliorations
*Quels sont les axes d'améliorations pour les personnes qui ont tenu les rôles de PO, SM et Dev sur la précédente itération?*
> Tout le monde a apprécié de se voir en groupe pendant le week-end.
> Penser sérieusement à ce qu'on peut faire si les APIs sont downs.

